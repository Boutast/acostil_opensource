#CC-BY-SA
#Arthur Costil

import os
import sys
from os import chdir

def main(argv):
    if (len(argv) > 0):
        if (argv[0] == "-h" or argv[0] == "--help" or argv[0] == "-help"):
            print("usage: Python CORPUS_DATASET")
            return (1)
        return (3)
    else :
        move_file()


def exist_file(filename):
    try:
        f = open(filename, 'r')
        f.close()
        return (1)
    except:
        return (0)

def move_file():
    chdir("./sources")
    list_dir = os.listdir('./')
    x = 0
    complex_test(list_dir, x)

def complex_test(list_dir, x):
    tmp = []
    while (x != len(list_dir) and x < len(list_dir)):
        open_test = exist_file(list_dir[x])
        FILE_DEST = open("FILE_DEST", "a")
        if (open_test == 1):
            fic = open(list_dir[x], "r")
            txt = fic.readlines()
            txt = ", ".join(txt)
            FILE_DEST.write(txt)
            FILE_DEST.write(" ")
            fic.close()
            x = x + 1
    FILE_DEST.close()

if __name__ =='__main__':
    main(sys.argv[1:])
